import * as mongoose from 'mongoose';

export const HistorySchema = new mongoose.Schema({
    id: Number,
    name: String,
    parent: String,
    status: String,
    description:String,
    client: String,
    areaResponsable: String,
    APS: String,
    type: String,
    BNA: String,
    members: String,
    total: Number,
    done: Number,
    percentageDone: Number,
    fechaGenerado: String
});

