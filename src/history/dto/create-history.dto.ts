export class CreateHistoryDto {
  readonly id: number;
  readonly name: string;
  readonly parent: string;
  readonly status: string;
  readonly description:string;
  readonly client: string;
  readonly areaResponsable: string;
  readonly APS: string;
  readonly type: string;
  readonly BNA: string;
  readonly members: string;
  readonly total: number;
  readonly done: number;
  readonly percentageDone: number;
  readonly fechaGenerado: string;
}