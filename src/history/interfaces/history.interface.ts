import { Document } from 'mongoose';

export interface History extends Document {
    id: number,
    name: string,
    parent: string,
    status: string,
    description:string,
    client: string,
    areaResponsable: string,
    APS: string,
    type: string,
    BNA: string,
    members: string,
    total: number,
    done: number,
    percentageDone: number,
    fechaGenerado: string
}
