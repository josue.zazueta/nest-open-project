import { Module } from '@nestjs/common';
import { HistoryController } from './history.controller';
import { HistoryService } from './history.service';
import { HistoryProviders } from './history.providers';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [HistoryController],
  providers: [HistoryService, ...HistoryProviders],
})
export class HistoryModule {}
