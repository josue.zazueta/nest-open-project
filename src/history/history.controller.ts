import { Controller, Get, Post, Body } from '@nestjs/common';
import { HistoryService } from './history.service';
import { History } from './interfaces/history.interface';

@Controller('history')
export class HistoryController {
  constructor(private readonly historyService: HistoryService) {}

  @Post()
  async (@Body() b: any){
    b.forEach(element => {
      this.historyService.create(element);
    });
  }

  @Get()
  async findAll(): Promise<History[]> {
    return this.historyService.findAll();
  }
}
