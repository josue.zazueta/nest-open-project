import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CreateHistoryDto } from './dto/create-history.dto';
import { History } from './interfaces/history.interface';

@Injectable()
export class HistoryService {
  constructor(@Inject('HISTORY_MODEL') private readonly historyModel: Model<History>) {}

  async create(createHistoryDto: CreateHistoryDto): Promise<History> {
    const createdHistory = new this.historyModel(createHistoryDto);
    return createdHistory.save();
  }

  async findAll(): Promise<History[]> {
    return this.historyModel.find().exec();
  }
}
