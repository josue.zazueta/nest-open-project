export type projectInfo = { 
    id: number;
    name: string;
    parent: string;
    status: string;
    description:string;
    client: string;
    Area_Responsable:string; //customField10
    APS:string;    //customField12
    type: string;
    BNA: string; //customField9
    members: any;
}

export type projectStatus = {
    id: number;
    name: string;
    parent: string;
    status: string;
    description:string;
    client: string;
    Area_Responsable: string;
    APS: string;
    type: string;
    BNA: string;
    members: any;
    total: number;
    done: number;
    percentageDone: number;
    fechaGenerado: any;

}
