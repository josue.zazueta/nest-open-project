import { Module } from '@nestjs/common';
import { ProjectStatusModule } from './projectsStatus/ProjectStatus.module';
import { EasyconfigModule } from 'nestjs-easyconfig';
import { HistoryModule } from './history/history.module';
import { WriterModule } from './csvWritter/writer.module';
@Module({
  imports: [ProjectStatusModule, HistoryModule, WriterModule, EasyconfigModule.register({path: './.env'})],
  controllers: [],
  providers: [],
})
export class AppModule {}
