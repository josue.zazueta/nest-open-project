import { Controller, Get, Res } from '@nestjs/common';
import { ProjectStatusService } from './ProjectStatus.service';
import { Response } from 'express';
@Controller('/project')
export class ProjectStatusController {

    @Get()
    async findAll(@Res() res: Response) {
        try {
        console.log(`[Server Message. [${Date()}] ] : Consumed Endpoint /project. `)
        const projects : ProjectStatusService = new ProjectStatusService()
        await projects.getProjects();
        res.sendStatus(200);
        }
        catch(err) {
            console.log(err)
            res.sendStatus(500);
        }
    }

}
    