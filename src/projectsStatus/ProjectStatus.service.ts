/* eslint-disable @typescript-eslint/camelcase */
import { Injectable } from '@nestjs/common';
import fetch from 'node-fetch';
import { Response } from 'node-fetch';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { projectInfo, projectStatus } from './../types/project';
import { IndexOfMatches } from './../utils/getMatchesFromArray'

// eslint-disable-next-line @typescript-eslint/camelcase
const URL_projects: string = process.env.URL_GET_PROJECTS;
const SERVER_URL: string = process.env.SERVER_URL;
const URL_WP: string = process.env.URL_WP;
const URL_WP2: string = process.env.URL_WP2;
const Password: string = process.env.PASS;
const URL_HISTORY: string = process.env.URL_HISTORY;
const URL_WRITE: string = process.env.URL_WRITE;

/**
 * @class 
 * 
 */
@Injectable()
export class ProjectStatusService {
    private getMatches:any
    constructor(){
        this.getMatches = new IndexOfMatches;
    }
    /**
     * @privateFunction
     * @param {String} url URL that will be consumed with get method. 
     * @param {String} m Method used in the request.
     * @return {Promise<Response>} This is the response got from the URL. 
     */
    private async getURL(url:string, method:string): Promise<Response>{
        const projectsFetch: Response = await fetch(url, {
            method: `${method}`,
            headers: {
            'Authorization': `Basic ${Password}`}  
        });
        return projectsFetch;
    }

    private async getTime(option:string): Promise<any> {
        const date_ob = new Date();
        const date = ("0" + date_ob.getDate()).slice(-2);
        const month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
        const year = date_ob.getFullYear();
        const hours = date_ob.getHours();
        const minutes = date_ob.getMinutes();
        const seconds = date_ob.getSeconds();
        if(option === "log"){
            return(year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds);
        }
        else{
            console.log(`${date}${month}${year}`)
            return(`${date}+${month}+${year}`);
        }
    }
    private async getLeader(project: string): Promise<any> {
        try {
            const members: any = [];
            const url = URL_projects + project;
            let fullInformation: any = await this.getURL(url,'get');
            fullInformation = await fullInformation.json();
            const membershipsURI: string = fullInformation._links.memberships.href;
            const membershipsURL: string = SERVER_URL + membershipsURI;
            let memberships: any = await this.getURL(membershipsURL, 'get');
            memberships = await memberships.json();
            memberships = memberships._embedded.elements;
            memberships.forEach(element => {
                const member: any = {
                    rol: element._links.roles[0].title,
                    name: element._links.principal.title
                };
                members.push(member);
            });
            return members;
        }
        catch(err){
            return null;
        }
    
    }
    /**
     * @privateFunction
     * @param {Object} json Javascript object that is going to be iterated
     *                      in order to get the information about the project
     *                      that is being requested. 
     * @return {Array}      Filtered array containing the projects information. 
     */ 
    private async filtrate ( json: any ) : Promise<Array<projectInfo>> {
        let projectsArray: any = [];
        let p: any;
        let parents:Array<any> = [];
        let toDelete:any = [];
        const array: any = json._embedded.elements;
        for(p of array){
            if(p._links.parent.title !== null){    
                parents.push(p._links.parent.title);
            }
            const members = await this.getLeader(p.identifier);
            const projectObject: any = {
                id: p.id,
                name: p.name,
                parent: p._links.parent.title,
                status: p.status,
                description: p.description.raw,
                client: p.customField1,
                Area_Responsable: p._links.customField10.title,
                APS: p.customField12,
                type: p._links.customField2.title,
                BNA: p.customField9,
                members: JSON.stringify(members)
                
            }   
                projectsArray.push(projectObject);
        }
        parents = parents.filter((value,index,self) =>{
            return self.indexOf(value) === index;
        });
        toDelete = await this.getMatches.getIndexesOfMatches(projectsArray, parents, 'name')
        for(const elementToDelete of toDelete ){
            delete projectsArray[elementToDelete]
        }
        projectsArray = projectsArray.filter(value => value !== undefined);
        console.log(`[Server Message. [${Date()}]] : Got all projects from OpenProject API.`);
        return projectsArray;
    }
    
    /**
     * @PrivateFunction
     * @param {Object} json Projects information previously gotten.
     * @return {Object} Projects status plus their workpackages info.
     */
    private async work_packages (json:Array<projectInfo>): Promise<Array<projectStatus>> {
        let percentageDone:number;
        const j:Array<projectInfo> = json;
        const projectStatus:any = [];

        for(const p of j){
            const projectObject:any = {
                id: p.id,
                name: p.name,
                parent: p.parent,
                status: p.status,
                description: p.description,
                client: p.client,
                Area_Responsable: p.Area_Responsable,
                APS: p.APS,
                type: p.type,
                BNA: p.BNA,
                members: p.members
            }
            const id:number = p.id;
            const url = `${URL_WP}${id}${URL_WP2}`;
            const twork_packages:any = await this.getURL(url,'get');
            const work_packages:any = await twork_packages.json();
            let counter = 0;
            const packages:any = work_packages._embedded.results._embedded.elements
            for(const i of packages){
                const done = i._links.status.title;
                if(done === 'Closed') counter++;
            }
            const total = work_packages._embedded.results.total;
            if(counter == 0){
                percentageDone = 0;
            }
            else{
                percentageDone = ((counter * 100) / total);
            }
            projectObject.total = total;
            projectObject.done = counter;
            projectObject.percentageDone = Math.round(percentageDone);
            projectObject.fechaGenerado = await this.getTime("status");                
            projectStatus.push(projectObject);
            }        
        return projectStatus;
        }
    
    /**
     * @privateFunction
     * @return {Array} All te project info the api is able to give.
     */
    // public async getProjects (): Promise<Array<projectStatus>> {
        public async getProjects (): Promise<any> {

        try {
            const projectsFetch: any = await this.getURL(URL_projects,'get')
            const projectsInfo: any = await projectsFetch.json();
            const res : Array<projectInfo> = await this.filtrate(projectsInfo);
            const statuses:any = await this.work_packages(res)
            await fetch(URL_HISTORY, {
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(statuses)
            });
            const allHistory:any = await fetch(URL_HISTORY)
            const json:any = await allHistory.json();
            await fetch(URL_WRITE,{
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(json)
            });
            return;
    }
        catch(err){
            console.log(`Error in project-status.service caused by :${err}`);
        }
    } 

}






