import { Module } from '@nestjs/common';
import { ProjectStatusController } from './ProjectStatus.controller';
import { ProjectStatusService } from './ProjectStatus.service';
@Module({
    imports: [],
    controllers: [ ProjectStatusController ],
    providers: [ ProjectStatusService ],
})
export class ProjectStatusModule {

}
