/**
 * @class This class exposes a function that takes 3 elements as parameters.
 *        The first one is an array of objects that has elements that contains
 *        elements with values that you dont want and you need to change them or
 *        just to punish and desintegrate that entire element of the array.
 *        The second is an array with elements that matchs with something specific
 *        in the objects contained in the first array like a name or number.
 *        and the third element refers to the object element where you are gonna
 *        look in to.
 * 
 */
export class IndexOfMatches {
    /**
     * @function
     * @param {Any} valueOfArrayToCompare Value of of the array that is gonna be compared
     * @param {Array} comparingElement    Array containing the guilty elements who are gonna be looked for
     * @return {boolean}                  Returns true when it finds a any guilty in the element given
     */
    private comparingFunction = (valueOfArrayToCompare:any,comparingElement:Array<any>) => {
        for(const index of comparingElement){
            if(valueOfArrayToCompare === index){
                return true;
            }
        }
    }
    /**
     * @function
     * @param {Array} arrayToCompare         This is an array with objects that contains elements that you dont want
     *                                       and obviously you are gonna look for to punish them
     * @param {Array} arrayContainingMatches Array containing the guilty elements who are gonna be looked for
     * @param {Any} elementToFind            This variable refers to the field of the object that is gonna be inspected
     */
    public getIndexesOfMatches = (arrayToCompare:Array<any>, arrayContainingMatches:Array<any>, elementToFind:any) => {
        const toDelete = [];
        const element = elementToFind;
        for(const indexOfMatch in arrayToCompare){
            if (this.comparingFunction(arrayToCompare[indexOfMatch][element], arrayContainingMatches)){
                toDelete.push(indexOfMatch);
                }
            }
        return toDelete;
    }
}

