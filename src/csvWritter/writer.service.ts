import { Injectable } from '@nestjs/common';
import { createObjectCsvWriter }  from 'csv-writer';

@Injectable()
export class WriterService {
    private csvWriter:any
    constructor(){
        this.csvWriter = createObjectCsvWriter({
            path: './openProject.csv',
            header: [
              {id: 'id', title: 'ID'},
              {id: 'name', title: 'Name'},
              {id: 'parent', title: 'Parent'},
              {id: 'status', title: 'Status'},
              {id: 'client', title: 'Client'},
              {id: 'Area_Responsable', title: 'Area_Responsable'},
              {id: 'APS', title: 'APS'},
              {id: 'type', title: 'Type'},
              {id: 'BNA', title: 'BNA'},
              {id: 'members', title: 'Members'},
              {id: 'total', title: 'Total'},
              {id: 'done', title: 'Done'},
              {id: 'percentageDone', title: 'PercentageDone'},
              {id: 'fechaGenerado', title: 'Date'},
            ]
          });
    }

    /**
     * @privateFunction
     * @return {void}
     */
    public getCSV(statuses:any):void {
        const data = statuses;
        this.csvWriter.writeRecords(data).
        then(() => console.log("The CSV file was written succesfully"));
        return;        
    }
  }