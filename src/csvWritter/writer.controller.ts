import { Controller, Post, Body, Res } from '@nestjs/common';
import { WriterService } from './writer.service';
@Controller('/write')
export class WriterController {
    @Post()
    async (@Body() b: any, @Res() res: any ){
        const Writer:WriterService = new WriterService()
        Writer.getCSV(b);
        res.sendStatus(200);
      }
}
    