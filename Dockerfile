FROM node:13.8
WORKDIR /app
COPY . .
RUN apt-get update
RUN npm install
RUN npm i -g @nestjs/cli
CMD [ "nest", "start" ]